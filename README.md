**Slide Scraper**

A simple python script I wrote to download all of the daily lecture slides for my
Object Oriented Programming in C++ course. This script has been scheduled to
run every Monday and Wednesday during class for the 2017 Spring semester 
using cron to schedule the task. 
