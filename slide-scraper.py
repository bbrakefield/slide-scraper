#!/usr/bin/python3
from bs4 import BeautifulSoup
from os.path import expanduser
import urllib.request
import requests
import os
import sys

class ColemanParser():
    """Class that will scrape Dr. Coleman's CS307 Today's Class web page."""
    def __init__(self,
        url='http://www.cs.uah.edu/~rcoleman/CS307/TodaysClass/TodaysClass.html',
        shorter_url='http://www.cs.uah.edu/~rcoleman/CS307/TodaysClass/',
        save_path=os.path.expanduser('~/')):

        self.url = url
        self.shorter_url = shorter_url
        self.save_path = save_path

    def download_page(self):
        """Send HTTP request, get html, and create a Beautiful Soup object."""
        r = requests.get(self.url)
        html_doc = r.text
        soup = BeautifulSoup(html_doc,"lxml")
        return soup

    def get_images(self, soup):
        """Find all images in the soup and collect their source"""
        images = []
        img_sauce = soup.find_all('img')
        for image in img_sauce:
            images.append(self.shorter_url + image.get('src'))
        return images

    def get_heading(self, soup):
        """Find all important headings and manually select the one that represents
        today's date
        """
        headings = []
        heading_sauce = soup.find_all('h1')
        for heading in heading_sauce:
            headings.append(''.join(heading.find_all(text=True)))
        return headings[1]

    def create_dir(self, date_folder):
        """If the specified directory at which to save images does not exist,
        create it.
        """
        if not os.path.exists(date_folder):
            os.makedirs(date_folder)

    def save_images(self, slides):
        """Get the resource ID from the url and save the slide under that name."""
        for slide in slides:
            filename = slide.split('/')[-1]
            print(slide)
            print(filename)
            try:
                urllib.request.urlretrieve(slide,filename)
            except urllib.error.HTTPError:
                print("Slide is not current available on site.")


def create_parser():
    """If a directory is specified as a command line argument, create a parser
    with that directory path set as the default value for save_path.
    If a path is not specified, save at the user's home directoy.
    """
    try:
        directory_name = sys.argv[1]
        def_parser = ColemanParser(save_path=directory_name)
        return def_parser

    except:
        print("Argument not found, using default destination.")
        def_parser = ColemanParser()
        return def_parser

def daily_procedure():
    """List of method calls that will perform the task that make up
    the daily procedure for scraping slides.
    """
    parser = create_parser()
    sauce = parser.download_page()
    slides = parser.get_images(sauce)
    date_folder = parser.save_path + parser.get_heading(sauce)
    parser.create_dir(date_folder)
    os.chdir(date_folder)
    parser.save_images(slides)

def main():
    daily_procedure();

if __name__ == '__main__':
    main()
